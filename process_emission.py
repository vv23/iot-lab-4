import json
import logging
import sys

import greengrasssdk

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
client = greengrasssdk.client("iot-data")

# Counter
vehicle_co2 = {}

def lambda_handler(event, context):
    #print("Hi lambda")
    #TODO1: Get your data
    vehicle = event['vehicle']
    co2 = event['co2']
    
    print(event)
    
    if vehicle in vehicle_co2.keys():
        vehicle_co2[vehicle] = max(vehicle_co2[vehicle], co2)
    else:
        vehicle_co2[vehicle] = co2
    
    print(vehicle_co2)
    
    #TODO2: Calculate max CO2 emission

    body={
        "message":"vehicle:{}|maxCO2:{}".format(vehicle, vehicle_co2[vehicle])
    }

    #TODO3: Return the result
    client.publish(
        topic="hello/world/{}".format(vehicle),
        
        payload=json.dumps(body),
    )

    return